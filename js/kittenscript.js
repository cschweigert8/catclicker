$(window).load(function(){

	var $catListElem = $('#cat-list-container');
	var $catContainerElem = $('#cat-container');

	//create empty array and load cats
	var cats = [];
	createCats(cats);

	// Let's loop over the numbers in our array
	for (var i = 0; i < cats.length; i++) {

		// get the first cat in the array
		var cat = cats[i];

		// We're creating a DOM element for the cats and adding them
		var elem = document.createElement('div');
		elem.textContent = cat.name;
		$catListElem.append(elem);

		// ... and when we click the cat, build the div for the clicker game
		elem.addEventListener('click', (function(catCopy) {
			return function() {
				//clear cat container
				$catContainerElem.empty();
				console.log("cat container emptied...");
				//add cat name
				$catContainerElem.append('<h3>'+catCopy.name+'</h3>');
				//add cat image
				$catContainerElem.append('<input type="image" id="cat-image"'+ 
					'src="'+catCopy.imgURL+'" height="200" width="300">');
				//add counter text
				var counter = catCopy.counter;
				$catContainerElem.append('<p id="counter">'+counter+'</p>');

				$('#cat-image').click(function(counter){
					counter = parseInt($("#counter").text(), 10) + 1;
					$("#counter").text(counter);
					return counter;
				});
			};
		})(cat));
	};
});

function createCats(cats){

	console.log("Creating cats...");
	var blackCat = {
		name: 'Black Cat',
		counter: 0,
		imgURL: 'img/black_cat.jpg'
	};
	cats.push(blackCat);

	var furryKitten = {
		name: 'Furry Kitten',
		counter: 0,
		imgURL: 'img/furry_kitten.jpg'
	};
	cats.push(furryKitten);

	var redCat = {
		name: 'Red Cat',
		imgURL: 'img/red_cat.jpg'
	};
	cats.push(redCat);

	var whiteCat = {
		name: 'White Cat',
		imgURL: 'img/white_cat.jpg'
	};
	cats.push(whiteCat);

	var fatCat = {
		name: 'Fat Cat',
		imgURL: 'img/fat_cat.jpg'
	};
	cats.push(fatCat);

	return cats;
}


/* $(window).load(function(){
	console.log("Setting names...");
	var blackCatName = "Smokey";
	$("#blackCatName").text(blackCatName);

	var furryKittenName = "Misty";
	$("#furryKittenName").text(furryKittenName);
});

$("#furryKitten").click(function(){

		var counter = parseInt($("#furryKittenCounter").text(), 10) + 1;
		$("#furryKittenCounter").text(counter);
	});

$("#blackCat").click(function(){

		var counter = parseInt($("#blackCatCounter").text(), 10) + 1;
		$("#blackCatCounter").text(counter);
	}); */